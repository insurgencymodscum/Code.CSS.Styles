# About #

CSS styles (AKA *userstyles*) for "Stylish" Chrome extension.  

# Install #

- With autoupdate

```
https://userstyles.org/users/352695
```

- Directly (*copy and paste*), no autoupdate

```
https://gitlab.com/insurgencymodscum/Code.CSS.Styles/tree/master
```

---

http://www.w3schools.com/cssref

http://www.tutorialrepublic.com/css-reference/css3-properties.php